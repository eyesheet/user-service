pub mod proto {
    tonic::include_proto!("eyesheet");
    pub mod users {
        tonic::include_proto!("eyesheet.users");
    }
}

mod auth_token;
mod db;

use argon2::password_hash::rand_core::OsRng;
use argon2::{Argon2, PasswordHash, PasswordHasher, PasswordVerifier};
use jsonwebtoken::TokenData;
use proto::users;
use proto::users::users_server::{Users, UsersServer};
use tonic::async_trait;

use sqlx::postgres::PgPoolOptions;
use uuid::Uuid;

use std::env;

struct UsersService {
    db_pool: sqlx::Pool<sqlx::Postgres>,
    argon2: Argon2<'static>,
}

#[async_trait]
impl Users for UsersService {
    async fn login(
        &self,
        request: tonic::Request<users::LoginRequest>,
    ) -> Result<tonic::Response<users::LoginResponse>, tonic::Status> {
        let message = request.get_ref();

        let username = &message.username;
        let password = &message.password;

        let row: Option<(sqlx::types::Uuid, String)> =
            sqlx::query_as("SELECT uuid, password FROM users WHERE username = $1")
                .bind(username)
                .fetch_optional(&self.db_pool)
                .await
                .map_err(|e| tonic::Status::unknown(e.to_string()))?;
        let user = row
            .filter(|(_, password_hash)| {
                PasswordHash::parse(&password_hash, argon2::password_hash::Encoding::B64)
                    .and_then(|password_hash| {
                        self.argon2
                            .verify_password(password.as_bytes(), &password_hash)
                    })
                    .is_ok()
            })
            .map(|(user, _)| user);

        if let Some(user) = user {
            let refresh_token: String = sqlx::query_as("INSERT INTO refresh_tokens(token, user_id, expiration) VALUES (DEFAULT, $1, $2) RETURNING token")
                .bind(user)
                .bind(chrono::Utc::now() + chrono::Duration::seconds(auth_token::REFRESH_TOKEN_EXPIRY_SECS))
                .fetch_one(&self.db_pool)
                .await
                .map(|t: (uuid::Uuid,)| t.0.to_string())
                .map_err(|_| tonic::Status::internal("db error"))?;

            let response = users::LoginResponse {
                access_token: auth_token::generate_access_token(user).await?,
                refresh_token,
            };
            Ok(tonic::Response::new(response))
        } else {
            Err(tonic::Status::unauthenticated(
                "No user was found with provided username and password.",
            ))
        }
    }

    async fn get_current_user(
        &self,
        request: tonic::Request<()>,
    ) -> Result<tonic::Response<users::User>, tonic::Status> {
        let token: &TokenData<auth_token::Claims> = request
            .extensions()
            .get()
            .ok_or(tonic::Status::unauthenticated("No auth token supplied"))?;
        let sub = sqlx::types::Uuid::parse_str(&token.claims.sub)
            .map_err(|_| tonic::Status::unauthenticated("Invalid JWT"))?;
        let row: (String,) = sqlx::query_as("SELECT username FROM users WHERE uuid = $1")
            .bind(sub)
            .fetch_optional(&self.db_pool)
            .await
            .map_err(|_| tonic::Status::internal("db error"))?
            .ok_or(tonic::Status::not_found("User was not found"))?;
        Ok(tonic::Response::new(users::User {
            user_id: sub.to_string(),
            username: row.0,
        }))
    }

    async fn create_user(
        &self,
        request: tonic::Request<users::CreateUserRequest>,
    ) -> Result<tonic::Response<()>, tonic::Status> {
        let message = request.get_ref();
        let username = &message.username;
        let password = &message.password;
        let salt = &argon2::password_hash::SaltString::generate(&mut OsRng);
        let password_hash = self
            .argon2
            .hash_password(password.as_bytes(), &salt)
            .map_err(|e| {
                dbg!(e);
                tonic::Status::unknown("password hash error")
            })?;
        sqlx::query("INSERT INTO users(username, password) VALUES ($1, $2)")
            .bind(username)
            .bind(password_hash.to_string())
            .execute(&self.db_pool)
            .await
            .map_err(|e| {
                e.as_database_error()
                    .and_then(|e| e.constraint())
                    .and_then(|constraint| {
                        (constraint == "users_username_key")
                            .then_some(tonic::Status::already_exists("User already exists"))
                    })
                    .unwrap_or(tonic::Status::unknown(e.to_string()))
            })?;
        Ok(tonic::Response::new(()))
    }

    async fn refresh_access(
        &self,
        request: tonic::Request<users::AccessRefreshRequest>,
    ) -> Result<tonic::Response<users::AccessRefreshResponse>, tonic::Status> {
        let refresh_token = request.into_inner().refresh_token;
        let refresh_token_uuid = uuid::Uuid::parse_str(&refresh_token)
            .map_err(|_| tonic::Status::invalid_argument("Invalid refresh token"))?;
        let row: Option<(sqlx::types::Uuid, chrono::DateTime<chrono::Utc>)> =
            sqlx::query_as("SELECT user_id, expiration FROM refresh_tokens WHERE token = $1")
                .bind(&refresh_token_uuid)
                .fetch_optional(&self.db_pool)
                .await
                .map_err(|_| tonic::Status::internal("db error"))?;
        let user_id_if_valid = row.and_then(|(user_id, expiration)| {
            let now = chrono::Utc::now();
            if now > expiration {
                None
            } else {
                Some(user_id)
            }
        });
        if let Some(user_id) = user_id_if_valid {
            Ok(tonic::Response::new(users::AccessRefreshResponse {
                access_token: auth_token::generate_access_token(user_id).await?,
                refresh_token,
            }))
        } else {
            Err(tonic::Status::unauthenticated("Invalid refresh token"))
        }
    }

    async fn revoke_all_sessions(&self, request: tonic::Request<()>) -> Result<tonic::Response<()>, tonic::Status> {
        let token: &TokenData<auth_token::Claims> = request
            .extensions()
            .get()
            .ok_or(tonic::Status::unauthenticated("No auth token supplied"))?;
        let sub = Uuid::parse_str(&token.claims.sub)
            .map_err(|_| tonic::Status::internal("invalid jwt structure"))?;
        sqlx::query("DELETE FROM refresh_tokens WHERE user_id = $1")
            .bind(sub)
            .execute(&self.db_pool)
            .await
            .map_err(|err| {
                eprintln!("db error while calling revoke_all_sessions: {err}");
                tonic::Status::internal("db error")
            })?;
        Ok(tonic::Response::new(()))
    }
}

fn interceptor(mut request: tonic::Request<()>) -> Result<tonic::Request<()>, tonic::Status> {
    auth_token::add_auth_token_extension(&mut request)?;
    Ok(request)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv::dotenv().ok();
    let max_connections = 5;
    let db_pool = PgPoolOptions::new()
        .max_connections(max_connections)
        .connect(&env::var("DATABASE_URL")?)
        .await?;
    sqlx::migrate!()
        .run(&db_pool)
        .await?;
    let users_service = UsersService {
        db_pool,
        argon2: Argon2::default(),
    };
    println!("Starting server");
    tonic::transport::Server::builder()
        .accept_http1(true)
        .layer((
                tower_http::cors::CorsLayer::very_permissive(),
                tonic_web::GrpcWebLayer::new(),
              ))
        .add_service(UsersServer::with_interceptor(users_service, interceptor))
        .serve(env::var("USERS_SERVICE_ADDR")?.parse()?)
        .await?;
    Ok(())
}
