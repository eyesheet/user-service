use std::env;

use thiserror::Error;
use tokio::fs::File;
use tokio::io::AsyncReadExt;

#[derive(Error, Debug)]
#[error("Invalid digest length")]
pub struct InvalidDigestLength(#[from] digest::InvalidLength);

impl From<InvalidDigestLength> for tonic::Status {
    fn from(_: InvalidDigestLength) -> Self {
        tonic::Status::invalid_argument("Invalid digest length")
    }
}

#[derive(Error, Debug)]
pub enum AuthorizationHeaderParsingError {
    #[error("Invalid authorization header format")]
    InvalidAuthorizationHeaderFormat,
    #[error(transparent)]
    InvalidDigestLength(#[from] InvalidDigestLength),
    #[error(transparent)]
    JwtError(#[from] jsonwebtoken::errors::Error),
    #[error(transparent)]
    ToStrError(#[from] tonic::metadata::errors::ToStrError),
    #[error(transparent)]
    IoError(std::io::Error),
}

impl From<AuthorizationHeaderParsingError> for tonic::Status {
    fn from(e: AuthorizationHeaderParsingError) -> Self {
        use tonic::Status;
        match e {
            AuthorizationHeaderParsingError::InvalidAuthorizationHeaderFormat => {
                Status::invalid_argument(e.to_string())
            }
            AuthorizationHeaderParsingError::InvalidDigestLength(e) => e.into(),
            AuthorizationHeaderParsingError::JwtError(e) => Status::invalid_argument(e.to_string()),
            AuthorizationHeaderParsingError::ToStrError(e) => {
                Status::invalid_argument(e.to_string())
            }
            AuthorizationHeaderParsingError::IoError(_) => todo!(),
        }
    }
}

fn token_str_from_header(header: &str) -> Result<&str, AuthorizationHeaderParsingError> {
    header
        .split(' ')
        .skip(1)
        .next()
        .ok_or(AuthorizationHeaderParsingError::InvalidAuthorizationHeaderFormat)
}

pub static ACCESS_TOKEN_EXPIRY_SECS: usize = 86400;
pub static REFRESH_TOKEN_EXPIRY_SECS: i64 = 365 * 24 * 60 * 60;
pub static ALGORITHM: jsonwebtoken::Algorithm = jsonwebtoken::Algorithm::RS384;

#[derive(Debug, thiserror::Error)]
pub enum KeyLoadingError {
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error(transparent)]
    JWT(#[from] jsonwebtoken::errors::Error),
}

impl From<KeyLoadingError> for AuthorizationHeaderParsingError {
    fn from(e: KeyLoadingError) -> Self {
        match e {
            KeyLoadingError::Io(e) => AuthorizationHeaderParsingError::IoError(e),
            KeyLoadingError::JWT(e) => AuthorizationHeaderParsingError::JwtError(e),
        }
    }
}

async fn get_private_pem() -> Result<Vec<u8>, std::io::Error> {
    let mut file = File::open(env::var("USERS_TOKEN_PRIVATE_PEM").unwrap()).await?;
    let mut out = Vec::new();
    file.read_to_end(&mut out).await?;
    Ok(out)
}

async fn get_public_pem() -> Result<Vec<u8>, std::io::Error> {
    let mut file = File::open(env::var("USERS_TOKEN_PUBLIC_PEM").unwrap()).await?;
    let mut out = Vec::new();
    file.read_to_end(&mut out).await?;
    Ok(out)
}

fn get_public_pem_sync() -> Result<Vec<u8>, std::io::Error> {
    let mut file = std::fs::File::open(env::var("USERS_TOKEN_PUBLIC_PEM").unwrap())?;
    let mut out = Vec::new();
    std::io::Read::read_to_end(&mut file, &mut out)?;
    Ok(out)
}

pub async fn load_encoding_key() -> Result<jsonwebtoken::EncodingKey, KeyLoadingError> {
    jsonwebtoken::EncodingKey::from_rsa_pem(&get_private_pem().await?).map_err(Into::into)
}

pub async fn load_decoding_key() -> Result<jsonwebtoken::DecodingKey, KeyLoadingError> {
    jsonwebtoken::DecodingKey::from_rsa_pem(&get_public_pem().await?).map_err(Into::into)
}

pub fn load_decoding_key_sync() -> Result<jsonwebtoken::DecodingKey, KeyLoadingError> {
    jsonwebtoken::DecodingKey::from_rsa_pem(&get_public_pem_sync()?).map_err(Into::into)
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Claims {
    pub exp: usize,
    pub sub: String,
}

fn token_from_authorization_header(
    key: &jsonwebtoken::DecodingKey,
    header: &tonic::metadata::MetadataValue<tonic::metadata::Ascii>,
) -> Result<jsonwebtoken::TokenData<Claims>, AuthorizationHeaderParsingError> {
    let header = header.to_str()?.to_owned();
    let validation = jsonwebtoken::Validation::new(ALGORITHM);
    let token = jsonwebtoken::decode(token_str_from_header(&header)?, &key, &validation)?;
    Ok(token)
}

pub fn add_auth_token_extension(request: &mut tonic::Request<()>) -> Result<(), tonic::Status> {
    let metadata = request.metadata();
    let key = load_decoding_key_sync()
        .map_err(|_| tonic::Status::internal("Error whilst loading key"))?;
    let auth_token = metadata
        .get("authorization")
        .map(|val| token_from_authorization_header(&key, val))
        .transpose()?;
    if let Some(v) = auth_token {
        request.extensions_mut().insert(v);
    };
    Ok(())
}

pub(crate) async fn generate_access_token(
    user_id: sqlx::types::Uuid,
) -> Result<String, tonic::Status> {
    let header = jsonwebtoken::Header::new(ALGORITHM);
    let claims = Claims {
        sub: user_id.to_string(),
        exp: jsonwebtoken::get_current_timestamp() as usize + ACCESS_TOKEN_EXPIRY_SECS,
    };

    let key = load_encoding_key()
        .await
        .map_err(|_| tonic::Status::internal("Error whilst loading key"))?;
    jsonwebtoken::encode(&header, &claims, &key).map_err(|e| tonic::Status::unknown(e.to_string()))
}
