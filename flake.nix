{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/master";
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, crane }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        craneLib = crane.lib.${system};
        protobufFilter = path: _type: builtins.match ".*proto$" path != null;
        migrationsFilter = path: _type: builtins.match ".*/migrations/.*sql$" path != null;
        sourcesFilter = path: type:
          (protobufFilter path type) || (migrationsFilter path type) || (craneLib.filterCargoSources path type);
      in
      rec {
        defaultPackage = craneLib.buildPackage {
          src = pkgs.lib.cleanSourceWith {
            src = craneLib.path ./.;
            filter = sourcesFilter;
          };
          nativeBuildInputs = with pkgs; [ protobuf postgresql pkg-config openssl ];
        };
        devShell = with pkgs; mkShell {
          inputsFrom = [ defaultPackage ];
          buildInputs = [ cargo rustc rustfmt pre-commit rustPackages.clippy cachix ];
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
        packages.dockerImage = let
          port = "43400";
        in pkgs.dockerTools.buildImage {
          name = "eyesheet-user-service";
          copyToRoot = pkgs.buildEnv {
            name = "image-root";
            paths = [ defaultPackage ];
            pathsToLink = [ "/bin" ];
          };
          config = {
            Cmd = [ "/bin/user-service" ];
            Env = [
              "USERS_SERVICE_ADDR=0.0.0.0:${port}"
            ];
            ExposedPorts = {
              "${port}" = {};
            };
          };
        };
      });
}
