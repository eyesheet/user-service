-- Add migration script here
CREATE TABLE IF NOT EXISTS users
(
	uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	username VARCHAR(255),
	password VARCHAR(255)
)
