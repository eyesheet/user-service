CREATE TABLE refresh_tokens
(
	token UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	user_id UUID REFERENCES users(uuid),
	expiration TIMESTAMP WITH TIME ZONE
)
