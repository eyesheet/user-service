#!/bin/sh

docker run --name eyesheet-user-db -e POSTGRES_PASSWORD=password -d -p 5432:5432 postgres
